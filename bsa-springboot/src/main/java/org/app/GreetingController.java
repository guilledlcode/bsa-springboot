package org.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controllers.Greeting;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class GreetingController {
	
	private static final Logger LOG = LogManager.getLogger(Application.class);

    /*@GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        
        LOG.info(">>>>>>>>>>>Controller>>>>>>>>>>>>>>>");
        
        return "greeting";
    }*/
	
	@GetMapping("/greeting")
    public String greetingForm(Model model) {
        model.addAttribute("greeting", new Greeting());
        
        LOG.info(">>>>>>>>>>>GetMapping greeting>>>>>>>>>>>>>>>");
        
        return "greeting";
    }
    
    @PostMapping("/greeting")
    public String greetingSubmit(@ModelAttribute Greeting greeting) {
    	
    	LOG.info(">>>>>>>>>>>PostMapping greeting>>>>>>>>>>>>>>>");
    	
        return "result";
    }
 
}
