package org.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controllers.Data;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.utils.Utils;

@Controller
public class DataController {
	private static final Logger LOG = LogManager.getLogger(Application.class);
	
	@GetMapping("/data")
    public String greetingForm(Model model) {
        model.addAttribute("data", new Data());
        
        LOG.info(">>>>>>>>>>>GetMapping data>>>>>>>>>>>>>>>");
        
        return "data";
    }
	
	@PostMapping("/data")
    public String greetingSubmit(@ModelAttribute("data") Data data, Model model) {
    	LOG.info(">>>>>>>>>>>PostMapping redirecciona a home>>>>>>>>>>>>>>>");
    	
    	JSONObject obj = new JSONObject();
    	
		obj.put("ambiente", data.getAmbienteId());
		obj.put("publickey", data.getDecPublicKey());
		obj.put("privatekey", data.getDecPrivateKey());
		obj.put("operationid", Utils.randidOperation());
		obj.put("currency", data.getCurrency());
		obj.put("userid", data.getUserId());
		obj.put("usermail", data.getUserMail());
		obj.put("paymentmethod", data.getPaymentMethod());
		obj.put("bin", data.getBin());
		obj.put("installments", data.getInstallments());
		obj.put("merchant", data.getTpMerchant());
		obj.put("security", data.getTpSecurity());
		obj.put("channel", data.getChannel());
		obj.put("operationtype", data.getOperationType());
		obj.put("currencycode", data.getCurrencyCode());
		obj.put("concept", data.getConcept());
		obj.put("amount", data.getAmount());
		obj.put("buyerpreselectionmediopago", data.getBuyerPreselectionMp());
		obj.put("buyerpreselectionbanco", data.getBuyerPreselectionBank());
		obj.put("availablepaymentmethods", data.getAvailablePaymentMethods());
		obj.put("availableEntities", data.getAvailableEntities());
		
		System.out.print(obj);
		
        return "home";
    }
}
