package org.app;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.utils.Utils;

@Controller
public class HomeController {
	private static final Logger LOG = LogManager.getLogger(Application.class);

	@GetMapping("/")
	public String home(Model model) {
        //model.addAttribute("name", name);
        
        LOG.info(">>>>>>>>>>>Home Controller>>>>>>>>>>>>>>>");
        
        
        return "home";
    }
	
}
