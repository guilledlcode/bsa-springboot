package org.controllers;

public class Data {
	private String ambienteId;
    private String decPublicKey;
    private String decPrivateKey;
    private String currency;
    private String userId;
    private String userMail;
    private String paymentMethod;
    private String bin;
    private String installments;
    private String establishmentName;
    
    private String tpMerchant;
    private String tpSecurity;
    private String channel;
    private String operationType;
    private String currencyCode;
    private String concept;
    private String amount;
    private String buyerPreselectionMp;
    private String buyerPreselectionBank;
    private String availablePaymentMethods;
    private String availableEntities;
    
    public String getAmbienteId() {
        return ambienteId;
    }

    public void setAmbienteId(String ambienteId) {
        this.ambienteId = ambienteId;
    }
    
    public String getDecPublicKey() {
        return decPublicKey;
    }

    public void setDecPublicKey(String decPublicKey) {
        this.decPublicKey = decPublicKey;
    }
    
    public String getDecPrivateKey() {
        return decPrivateKey;
    }

    public void setDecPrivateKey(String decPrivateKey) {
        this.decPrivateKey = decPrivateKey;
    }
    
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }
    
    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }
    
    public String getInstallments() {
        return installments;
    }

    public void setInstallments(String installments) {
        this.installments = installments;
    }
    
    public String getEstablishmentName() {
        return establishmentName;
    }

    public void setEstablishmentName(String establishmentName) {
        this.establishmentName = establishmentName;
    }
    
    public String getTpMerchant() {
        return tpMerchant;
    }

    public void setTpMerchant(String tpMerchant) {
        this.tpMerchant = tpMerchant;
    }
    
    public String getTpSecurity() {
        return tpSecurity;
    }

    public void setTpSecurity(String tpSecurity) {
        this.tpSecurity = tpSecurity;
    }
    
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
    
    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }
    
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
    
    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }
    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    public String getBuyerPreselectionMp() {
        return buyerPreselectionMp;
    }

    public void setBuyerPreselectionMp(String buyerPreselectionMp) {
        this.buyerPreselectionMp = buyerPreselectionMp;
    }
    
    public String getBuyerPreselectionBank() {
        return buyerPreselectionBank;
    }

    public void setBuyerPreselectionBank(String buyerPreselectionBank) {
        this.buyerPreselectionBank = buyerPreselectionBank;
    }
    
    public String getAvailablePaymentMethods() {
        return availablePaymentMethods;
    }

    public void setAvailablePaymentMethods(String availablePaymentMethods) {
        this.availablePaymentMethods = availablePaymentMethods;
    }
    
    public String getAvailableEntities() {
        return availablePaymentMethods;
    }

    public void setAvailableEntities(String availableEntities) {
        this.availableEntities = availableEntities;
    }
}
